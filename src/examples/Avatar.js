import React, { useEffect } from 'react';
import { Avatar } from '@wedgekit/core';
import Layout from '@wedgekit/layout';

const Accordions = () => {
  useEffect(() => {
    window.parent.postMessage({
      action: 'resize',
      height: document.body.scrollHeight,
    }, 'https://wedgekit.wedgeworks.io');
  }, []);

  return (
    <Layout.Grid areas={[]} columns={['repeat(3, minmax(0, max-content))']} justify="center" align="center">
      <Avatar color="T100" size="large">Matt McElwee</Avatar>
      <Avatar color="T200">3</Avatar>
      <Avatar color="T400" size="small">John Brown</Avatar>
    </Layout.Grid>
  );
};

export default Accordions;
