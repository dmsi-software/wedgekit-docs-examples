import React, { useEffect } from 'react';
import { Button } from '@wedgekit/core';
import Layout from '@wedgekit/layout';
import { Route } from 'react-router-dom';
import styled from 'styled-components';

const Center = styled.div`
  width: max-content;
  margin: 0 auto;
`;

const Background = styled(Layout.Grid)`
  padding: 8px;
  background: #000;
  box-sizing: border-box;
`;

const Buttons = () => {
  useEffect(() => {
    window.parent.postMessage({
      action: 'resize',
      height: document.body.scrollHeight,
    }, 'https://wedgekit.wedgeworks.io');
  }, []);

  return (
    <>
      <Route path="/buttons/primary">
        <Layout.Grid areas={[]} columns={[1]} justify="center" align="center">
          <Center>
            <Button domain="primary">Click Me!</Button>
          </Center>
        </Layout.Grid>
      </Route>
      <Route path="/buttons/default">
        <Layout.Grid areas={[]} columns={[1]} justify="center" align="center">
          <Center>
            <Button>Click Me!</Button>
          </Center>
        </Layout.Grid>
      </Route>
      <Route path="/buttons/success">
        <Layout.Grid areas={[]} columns={[1]} justify="center" align="center">
          <Center>
            <Button domain="success">You Clicked Me!</Button>
          </Center>
        </Layout.Grid>
      </Route>
      <Route path="/buttons/danger">
        <Layout.Grid areas={[]} columns={[1]} justify="center" align="center">
          <Center>
            <Button domain="danger">Don't Click Me!</Button>
          </Center>
        </Layout.Grid>
      </Route>
      <Route path="/buttons/warning">
        <Layout.Grid areas={[]} columns={[1]} justify="center" align="center">
          <Center>
            <Button domain="warning">Maybe Don't Click Me!</Button>
          </Center>
        </Layout.Grid>
      </Route>
      <Route path="/buttons/white">
        <Background areas={[]} columns={[1]} justify="center" align="center">
          <Center>
            <Button domain="white">Click Me!</Button>
          </Center>
        </Background>
      </Route>
    </>
  );
};

export default Buttons;
