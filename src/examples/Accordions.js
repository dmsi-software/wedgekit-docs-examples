import React, { useState, useEffect } from 'react';
import { Accordion } from '@wedgekit/core';
import Layout from '@wedgekit/layout';

const Accordions = () => {
  const [open, setOpen] = useState(false);

  useEffect(() => {
    window.parent.postMessage({
      action: 'resize',
      height: document.body.scrollHeight,
    }, 'https://wedgekit.wedgeworks.io');
  }, [open]);

  return (
    <Layout.Grid areas={[]} columns={[1]} justify="center" align="center">
      <Accordion
        title="Top 5 Hobbits"
        open={open}
        onToggle={() => setOpen(!open)}
        id="not-a-piano"
      >
        {({ labelID, regionID }) => (
          <div role="region" aria-labelledby={labelID} id={regionID}>
            <ol>
              <li>
                <img src="https://media2.giphy.com/media/105OwsN7a4UQ2Q/giphy.gif?cid=790b7611c36154ead57f2e0a48268063fdcc3a4d10c5d3a2&rid=giphy.gif" />
              </li>
              <li>
                <img src="https://media2.giphy.com/media/hjiRxDYJcc3ny/giphy.gif?cid=790b761187837d1e66f278f660de87650b1100de52149324&rid=giphy.gif" />
              </li>
              <li>
                <img src="https://media1.giphy.com/media/6g6fIvdYMZPws/giphy.gif?cid=790b76119eb6edafb245edb28761b7fa21736b4612aa22af&rid=giphy.gif" />
              </li>
              <li>
                <img src="https://media1.giphy.com/media/bzFtsbvHDuGJi/giphy.gif?cid=790b761138f1944065913835b8f5bf400c75ceacba19100e&rid=giphy.gif" />
              </li>
              <li>
                <img src="https://media2.giphy.com/media/vZbNOemuoggNy/giphy.gif?cid=790b76112714aea5c7f4045f28c7aae66e996d54a33e36ad&rid=giphy.gif" />
              </li>
            </ol>
          </div>
        )}
      </Accordion>
    </Layout.Grid>
  );
};

export default Accordions;
