import React, { useEffect, useState } from 'react';
import { ErrorBoundary, Button } from '@wedgekit/core';
import { Route } from 'react-router-dom';

const Alerts = () => {
  const [rerender, setRerender] = useState(0);

  useEffect(() => {
    window.parent.postMessage({
      action: 'resize',
      height: document.body.scrollHeight,
    }, 'https://wedgekit.wedgeworks.io');
  }, [rerender]);

  return (
    <>
      <Route path="/alerts/critical-error">
        <ErrorBoundary
          title="My line has ended!"
          message="You sent your last living heir into battle with no plan, no funds, and a meager fighting force merely to sate your own warped messiah-complex."
        >
          <div>
            <Button
              onClick={() => {
                setRerender(Math.random());
                throw new Error('Uh oh!');
              }}
            >
              Send Faramir into battle
            </Button>
          </div>
        </ErrorBoundary>
      </Route>
    </>
  );
};

export default Alerts;
