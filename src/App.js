import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';

import Accordions from './examples/Accordions';
import Alerts from './examples/Alerts';
import Avatar from './examples/Avatar';
import Buttons from './examples/Buttons';

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/accordions">
          <Accordions />
        </Route>
        <Route path="/avatar">
          <Avatar/>
        </Route>
        <Route path="/buttons">
          <Buttons />
        </Route>
        <Route path="/alerts">
          <Alerts />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
